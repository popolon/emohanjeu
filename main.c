/* Many thanks to http://www.tldp.org/HOWTO/NCURSES-Programming-HOWTO/ */

#include <stddef.h>
#include <stdbool.h>
#include <sys/time.h>
#include <locale.h>
#include <ncurses.h>
#include <time.h>
#include <unistd.h>

#include "element.h"
#include "actor.h"
#include "background.h"
#include "level.h"
#include "playground.h"

uint8_t debug_display = 0;

// 1000k = 1 s (1/10s) 250k1/4s
useconds_t delay_time = 200000;

actor_elt perso, pperso;

actor_elt *meet;

WINDOW *pg;

unsigned int tab = 0;

#define LOG_NAME "/tmp/chineseascii.txt"
//
// ELEMENT WINDOW
// 

// ELT_WITH = WIDTH * 2 (UTF8)
#define ELT_WIN_WIDTH 4
#define ELT_WIN_HEIGHT ((ELT_COUNT + 1) / 2)

WINDOW *elementswin;

background_block *blocks_main;

void elements_window_create( uint16_t posx, uint16_t posy) {
  // newwin(height, width, starty, startx);  (UTF子=>WIDTH * 2)
  elementswin = newwin(ELT_WIN_HEIGHT, ELT_WIN_WIDTH, posy, posx);
}

void elements_window_delete() {
 delwin(elementswin);
}

void elements_window_draw() {
  for (unsigned int i = 0; i < ELT_COUNT; i++) {
    wattron(elementswin, COLOR_PAIR(i+1));
    mvwprintw( elementswin, i % ELT_WIN_HEIGHT, (i / ELT_WIN_HEIGHT) *2, blocks_main[i].hanzi);
//    mvwprintw( elementswin, i % ELT_WIN_HEIGHT, (i / ELT_WIN_HEIGHT) *2, "%02x", i);
//    mvwprintw( elementswin, i, (i / ELT_WIN_HEIGHT), "%02x", i % ELT_WIN_HEIGHT);
    wattroff(elementswin, COLOR_PAIR(i+1));
  }
  wrefresh(elementswin);
}

//
// SCOREBOARD WINDOW
//
void scoreboard_draw(unsigned int ch) {
  move(0,0);
  if ( debug_display == 1 ) {
    attron(A_REVERSE);
    mvprintw(0,0,"x:%3i y:%2i ", perso.x, perso.y);
    printw("key:%4i (%c|%C) tab:%i          ", ch, ch, ch, tab);
    attroff(A_REVERSE);
  }
  for (uint8_t i=0; i < 5; i++)
    if (i < perso.lives)
      printw(LIVE);
    else
      printw("  ");
  printw(" SCR:%5d", perso.score);
  printw(" LVL:%3d", level_cur);
  if ( debug_display == 1 )
    printw(" t:%d", time(NULL));
}

//
// MAIN
//
int main(int argc, char *argv[]) {
  char *nan = "男";
  char *yu = "🐟";
  char *zhu = "🐷";
//  wchar_t zhu = L'🐷';

  level_cur = 0;
 
  perso.x = pperso.x = 0;
  perso.y = pperso.y = 0;
  perso.emoji = zhu;
  perso.lives = 3;
  perso.score = 0;

  // Allow to display UTF8 chars
  setlocale(LC_ALL, "");

// wchar_t chars[4] = {L"a", L"b", L"v", L"d"};
  unsigned int ch = 32;

  log_init(LOG_NAME);

// *wstring = L'你好';

  // init screen
  initscr();			/* Start curses mode 		  */
  cbreak(); //???
//  raw();				/* Line buffering disabled	*/
  keypad(stdscr, TRUE);		/* We get F1, F2 etc..		*/

  // after windows creations
  start_color();			/* Start color 			*/
  /* See man 3 curs_inopts, curs_util and curs_getch */
  curs_set(0);              /* Hide the cursor */
  noecho();			        /* Don't echo getch */
  cbreak();					/* don't buffer waiting for nl or cr */
  nonl();                   /* no line jump */
  nodelay(stdscr, TRUE);    /* non-blocking getch */

  // create color pairs, need to initialise current_ first
  elements_colors_init();

  // initialse le niveau
  level_elt *level = level_init(level_cur);

  // get the current blockset /*fixme should be take by playground  to the level instead ? */
  blocks_main = get_background_blocks();

  // elements palette for editor
  elements_window_create( (PGV_WIDTH * 2) + 1, 0);

  background *bg = get_level_bg(level_cur);

  log_print("main adr level(%i) = %i level_cur: %i\n", level_cur, (long int) level);
//  log_print("main level1_elt = %i, obj_cnt = %i, act_count = %i\n", (long int) &level1_elt, level1_elt.object_count, level1_elt.actor_count);
//  log_print("main level1_elt.bg.width = %i, .bg.height = %i\n",  level1_elt.bg->width, level1_elt.bg->height);

  log_print("main bg adr: %i, bg->width: %i, bg->height: %i\n", (long int) bg, bg->width, bg->height );
//  current_level_elt = &level1_elt;

  // game playground  
//  playground_window_create( levels_elt[level_cur]);
  pg = playground_window_create( level);

//  log_print("main level1_elt = %i\n", (long int) &level1_elt);
//  log_flush();

  fish = actor_create(ACTOR_TYPE_FISH, bg);
  lamb = actor_create(ACTOR_TYPE_LAMB, bg);
  wolf = actor_create(ACTOR_TYPE_WOLF, bg);

  //TODO
  // actor_create(1, &level1_bg)

  // tests
  //mvprintw(1,0, "大家好!!!");	/* Print Hello World		  */
  //printw("%s",chars[0]);
  //printw("%C", L'男');
  //printw("%C", nan); /* %c =>7 %C => 男 */
  //addch(nan); ///marche pas avec utf, affiche 7//
  //printw("%C", yu);


/// If score bar is drawn playground isn't drawn ???

  // Score bar
  log_print("main: init scoreboard draw(ch)\n");  
  scoreboard_draw(ch);

  // update Playground window buffer
  log_print("main: init playground_window_fill()\n");  
  playground_window_fill();
  log_print("main: init playground_objects_draw()\n");  
  playground_objects_draw();
//  playground_actors_draw();
  log_print("main: init playground_actor_draw(fish)\n");  
  playground_actor_draw(fish);
  log_print("main: init playground_actor_draw(wolf)\n");  
  playground_actor_draw(wolf);

  log_print("main: init mvwprintw(playgroundwin, perso)\n");  
  mvwprintw( pg, perso.y, perso.x * 2, perso.emoji);

  log_print("main: init refresh\n");  

  wrefresh( pg); /* Print playground on screen */
  elements_window_draw();  /* Print elements on screen */
  refresh();   /* Print default window on to the real screen */

  // initialize chronometer (1 second precision)
  uint16_t cur_time = time(NULL);
  uint16_t prev_time = cur_time;
  
  
// *******************
// *******************
//     main loop
// *******************
// *******************
//getch();			/* Wait for user input */
  while((ch = getch()) != 27) { // 27 = ESC (1s delay)

// *******************
// events managements
// *******************

	// save former position for cleaning
	pperso.x = perso.x;
	pperso.y = perso.y;

	switch(ch) {
	  case KEY_LEFT:
        if ( perso.x > 0 && !background_tile_blocking( bg,  perso.x - 1, perso.y) )
          perso.x--;
        break;
      case KEY_RIGHT:
        if ( perso.x < PGV_WIDTH -1 && !background_tile_blocking( bg, perso.x + 1, perso.y) )
          perso.x++;
        break;
      case KEY_UP:
        if ( perso.y > 0 && !background_tile_blocking( bg, perso.x, perso.y - 1) )
          perso.y--;
        break;
      case KEY_DOWN:
        if ( perso.y < PGV_HEIGHT -1 && !background_tile_blocking( bg, perso.x, perso.y + 1) )
          perso.y++;
        break;
      case '\t': // \t == 9 = hw tab key
        tab=1;
        break;
      case 353: // (marchpas \b ?) == 353 = hw back-tab key
        // backtab=1;
        tab=2;
        break;
//      case KEY_STAB: //syskey KEY_BTAB = back-tab
//        tab=1; marche pas
//        break;
    }
    flushinp(); /* flush out input buffer */

    playground_object_collide(perso.x, perso.y, &perso);
    actor_action(wolf);

    cur_time = time(NULL);
    if ( prev_time != cur_time) {
	  actor_action(fish);
	  prev_time = cur_time;
    }
    
    meet = playground_character_collide(perso.x, perso.y, wolf);
    if ( meet == wolf ) {
      perso.lives--;
      if ( perso.lives == 0 )
		break;
    }

// *****************
// dreaw everything
// *****************

    // Score bar
    scoreboard_draw(ch);

    // update Playground window buffer
    playground_window_fill(); // background
    playground_objects_draw(); // objects

////    playground_actors_draw();
    playground_actor_draw(fish);
    playground_actor_draw(lamb);
    playground_actor_draw(wolf);

// delete former, see ncurses doupdate (man refresh) for optimisations
//    if ( pperso.x != perso.x || pperso.y != perso.y ) {
////       mvwprintw(playgroundwin, pperso.y, pperso.x * 2, "  ");

    // draw perso, to add : test for blink for few seconds after killed 
    // wattron(playgroundwin, A_BLINK);
    mvwprintw( pg, perso.y, perso.x * 2, perso.emoji);
    // wattroff(playgroundwin, A_BLINK);

    wrefresh( pg); /* Print playground on screen */
    elements_window_draw();  /* Print elements on screen */
    refresh();   /* Print default win on to the real screen */

    usleep(delay_time); /* sleep delay_time */
  }
  
  endwin();			/* End curses mode */

  playground_window_delete();
  elements_window_delete();

  log_close();

  return 0;
}
