CC = gcc
LDFLAGS = -lncurses
OBJS = main.c element.c background.c actor.c level.c playground.c

all: test_color emohanjeu ncurses_utf8

ncurses_utf8:
	${CC} ${LDFLAGS} ncurses_utf8.c -o ncurses_utf8

test_color:
	${CC} ${LDFLAGS} test_color.c -o test_color

emohanjeu:
	${CC} ${LDFLAGS} ${OBJS} -o emohanjeu
