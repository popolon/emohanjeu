#include "background.h"

// return the tile at position x,y
uint16_t background_tile(background *bg, uint16_t x, uint16_t y) {
  return bg->tiles[y * bg->width + x];
}

// return the tile at position x,y (bound test)
uint16_t background_tile_bounded(background *bg, uint16_t x, uint16_t y) {
  if ( x < 0 || y < 0 || x >= bg->width || y >= bg->height )
    return FALSE;
  return bg->tiles[y * bg->width + x];
}

// is the tile at position x,y of type (test background bounds)
bool background_tile_type(background *bg, uint16_t x, uint16_t y, uint16_t type) {
  // verify that the coords is existing in current table
  if ( x < 0 || y < 0 || x >= bg->width || y >= bg->height )
    return FALSE;
  uint16_t tile = background_tile( bg, x, y);
  if ( tile != 0xFFFF && is_background_bloc_of_type( tile, type) )
    return TRUE;
  else
    return FALSE;
}

// is the tile at position x,y of type (don't test background  bounds)
bool background_tile_type_notbounded(background *bg, uint16_t x, uint16_t y, uint16_t type) {
  uint16_t tile = background_tile( bg, x, y);
  if ( tile != 0xFFFF && is_background_bloc_of_type( tile, type) )
    return TRUE;
  else
    return FALSE;
}

// is the tile at position x,y of type
bool background_tile_blocking(background *bg, uint16_t x, uint16_t y) {
  uint16_t tile = background_tile( bg, x, y);
//  if ( tile == 8 || tile == 17)
  if ( is_background_bloc_of_type( tile, BGTYPE_BLOCKER) || is_background_bloc_of_type( tile, BGTYPE_BUILDING) || is_background_bloc_of_type( tile, BGTYPE_WATER) )
    return TRUE;
  else
    return FALSE;
}
