# Emohanjeu

Emohanjeu (\e̞˨.mo̞˦ xa̠n˥˧ ʒø\\) is an [ncurses](https://invisible-island.net/ncurses/), text terminal based interactive demo, mixing emoji for object and (game) characters and chinese han (script) characters (han characters is one among numerous scripts in China) (in CJKV: Hanzi/kanji/hanja/Hán tự) for background.

![emohanjeu.gif](emohanjeu.gif)

* [Game Rules](#Game Rules)
* [Build](#Build)
* [Name etymology](#Name etymology)
* [License](#License)

# Game Rules

* Simply use keys to move
* Press esc key or feed the wolf with your body 3 lives to quit the game

That's just an interactive demo, not a game.

# Objects
Objects are defined in level.c

: Tip: Main character is alcoholic and like apples

* 🍒: cherry, 20 points
* 🍓: strawberry, 50 points
* 🍎: apple, 100 points
* 🍚: rice bowl, 300 points
* 🍜: noodle bowl, 300 points
* 🍺: bier, 500 points
* 🍻: pair of bíers (双啤酒：干杯)
* 🍷: wine, 800 points
* 🥃: calvas (calvados 苹果白酒), 1600 points

# Characters

* 🐷: pig, the main character
* 🐟: fish, just a fish swiming in the river
* 🐺: wolf, warning, it walks in the forest and is hungry
* 🐏: sheep, a peacful and good friend
* 🏇: horse, a peacful friend, used as tool by humans
* 🐯: tiger, the king of the forest with the 王 character on it's front.

# Background tiles
Most hanzi are here are pictographics and some are ideographic or ideophonographic. They are all defined in element.c, but here are the essential ones for game :

## Vegetation
* 草 cǎo: grass
* 森 sēn: forest
* 林 lín: little wood
* 木 mù: wood (or long time ago, tree)
* 树 shù tree, most common today (trad. 樹)

## Shanshui (landscape)
* 山 shān: mountain
* 沙 shā: sand
* 石 shí: stone
* 河 hé: river
* 川 chuān: river
* 江 jiāng: river
* 池 chí: pond
* 湖 hú: lake
* 海 hǎi: sea

## Cities
* 城 chéng: city
* 墙 qiáng: wall
* 桥 qiáo: bridge (trad. 橋)
* 家 jiā: home
* 酒 jiǔ: alcohol (here used for bar
* 馆 guǎn: palace (trad. 館),
* 门 mén: door (trad. 門)

# Build
### Requirements
Need ncurses:
Debian based system : libncurses-dev
Archlinux based system: ncurses

An UTF8 font including basic Chinese characters (Chinese, Japanese or some Korean or Vietnamese (still strongly used at the same time than their new writings until ~1950 in both countries). I use simplified chinese here could add an option to choose between traditionnal and chinese. Japanese kanji are also since 20th century a mix of traditionnal (語,風,餃,…) and simplified (画，学，国…) hanzi.

## Compilation
./compile.sh

Sorry didn't practized c for long time, didn't managed/take time to have a working Makefile.

## Name etymology

Emohanjeu (\e̞˨.mo̞˦ xa̠n˥˧ ʒø\ is a multingual name, coming from Japanese: emoji + Mandarin Han Chinese: hanzi + French: jeu)

### Emo from emoji
Japanese emoji (絵文字／えもじ \e̞˨.mo̞˦.dʑi˨.◌˨\, meaning "picture character") come from:

* e (絵 [e̞], meaning "picture", "drawing", from Middle Chinese 繪 /ɦuɑiH/), from Old Chinese, Zhengzhang Shangfang reconstruction: /*ɡoːbs/.
* moji (文字 [mõ̞ɲ̟d͡ʑi], script, character), from Han Chinese word 文字 (script, character, pronounced \u̯ən˧˥ t͡sz̩˥˩\ in Modern Standard Mandarin) both from Old Chinese, Zhengzhang Shangfang reconstruction: /*mɯn zlɯs/.
* emo suggest me also English emoticon (or smiley) from emotion + icon, both from Old French, from Latin "emotus" (From ex- (“out of”) +‎ moveō (“move”), Proto-Indo-European:  *mew- ) and icon from Ancient Greek εἰκών (eikṓn), from Proto-Indo-European: *weyk-).

### Han from hanzi from Han
Mandarin Han Chinese hanzi (汉字/漢字 \xa̠n˥˧ t͡sz̩˥˩\, meaning Han character) come from Old Chinese.

* In Middle Chinese pronunciation \hɑnᴴ d͡zɨᴴ\
* Zhengzhang Shangfang Old Chinese phonetic reconstruction: /*hnaːns *zlɯs/

### Jeu from jocus
French jeu (\ʒø\, meaning game) come from Latin jocus \ˈjo.kus\, that obviously give "joke" in English) from Proto-Indo-European reconstruction: *yokos)

# License

Copyleft Popolon 2019-2021.

Licence GNU GPLv3

I'm not responsible if anything doesn't work or the whole universe collapse.

