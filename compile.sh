#!/bin/bash
gcc -o chinasciiart \
    main.c \
    element.c \
    background.c \
    actor.c \
    level.c \
    playground.c \
    -lncurses \
    # -Wall
