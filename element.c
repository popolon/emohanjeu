#include "element.h"

char *LOGNAME;
FILE *LOGFILE;

background_block background_blocks[] = {
    // vegetation
//	{ L'草', GREEN, LIGHTGREEN}, // cǎo
//	{ L'森', KAKI, DEEPGREEN}, // sēn
//	{ L'林', KAKI, LIGHTGREEN}, // lín
//	{ L'木', KAKI, LIGHTBROWN}, // mù
//	{ L'树', KAKI, BROWN}, // shù

	{ "草", COLOR_GREEN, COLOR_BLACK, BGTYPE_FLAT}, // cǎo
	{ "森", COLOR_BLACK, COLOR_GREEN, BGTYPE_WOOD}, // sēn
	{ "林", COLOR_BLACK, COLOR_GREEN, BGTYPE_WOOD}, // lín
	{ "木", COLOR_RED, COLOR_GREEN, BGTYPE_WOOD}, // mù
	{ "树", COLOR_RED, COLOR_GREEN, BGTYPE_WOOD}, // shù (樹)
    // 5

    // shanshui
//	{ L'山', GRAY, WHITE}, // shān
//	{ L'沙', LIGHTYELLOW, BROWN}, // shā
//	{ L'石', LIGHTGRAY, DEEPGRAY}, // shí
//	{ L'河', LIGHTGREEN, DEEPBLUE}, // hé
//	{ L'川', LIGHTGREEN, DEEPBLUE}, // chuān
//	{ L'江', LIGHTGREEN, DEEPBLUE}, // jiāng
//	{ L'池', LIGHTGREEN, DEEPBLUE}, // chí
//	{ L'湖', LIGHTGREEN, DEEPBLUE}, // hú
//	{ L'海', LIGHTGREEN, DEEPBLUE}, // hǎi

	{ "山", COLOR_BLACK, COLOR_WHITE, BGTYPE_MOUNT}, // shān
	{ "沙", COLOR_BLACK, COLOR_YELLOW, BGTYPE_FLAT}, // shā
	{ "石", COLOR_BLACK, COLOR_WHITE, BGTYPE_BLOCKER}, // shí
	{ "河", COLOR_BLACK, COLOR_CYAN, BGTYPE_WATER}, // hé
	{ "川", COLOR_WHITE, COLOR_BLUE, BGTYPE_WATER}, // chuān
	{ "江", COLOR_WHITE, COLOR_BLUE, BGTYPE_WATER}, // jiāng
	{ "池", COLOR_WHITE, COLOR_BLUE, BGTYPE_WATER}, // chí
	{ "湖", COLOR_WHITE, COLOR_BLUE, BGTYPE_WATER}, // hú
	{ "海", COLOR_WHITE, COLOR_BLUE, BGTYPE_WATER}, // hǎi
    // + 9 = 14
    
	// cities
//	{ L'城', DEEPGRAY, LIGHTGRAY}, // chéng
//	{ L'墙', DEEPGRAY, LIGHTGRAY}, // qiáng
//	{ L'桥', DEEPGRAY, LIGHTGRAY}, // qiáo
//	{ L'家', RED, BROWN}, // jiā
//	{ L'门', RED, BROWN}, // mén (門)
//	{ L'馆', RED, YELLOW} // guǎn (館)

	{ "城", COLOR_WHITE, COLOR_BLACK, BGTYPE_FLAT}, // chéng
	{ "墙", COLOR_WHITE, COLOR_BLACK, BGTYPE_BUILDING}, // qiáng
	{ "桥", COLOR_YELLOW, COLOR_BLACK, BGTYPE_BRIDGE}, // qiáo (橋)
	{ "家", COLOR_RED, COLOR_WHITE, BGTYPE_BUILDING}, // jiā
	{ "酒", COLOR_YELLOW, COLOR_WHITE, BGTYPE_BUILDING}, // jiǔ
	{ "馆", COLOR_YELLOW, COLOR_WHITE, BGTYPE_BUILDING}, // guǎn (館),
	{ "门", COLOR_RED, COLOR_WHITE, BGTYPE_BUILDING}, // mén (門)
	// + 7 = 21

//	{ L'图书', RED} // túshū  <= idée, animer (alterner) entre les différents caractères de la chaîne
};

element_block sprite[] = {
	// crowd
	{ "男", TRANSPARENT, FREE }, // nán
	{ "女", TRANSPARENT, FREE }, // nǚ
	{ "孩", TRANSPARENT, FREE }, // hái
	{ "子", TRANSPARENT, FREE }, // zǐ
	{ "儿", TRANSPARENT, FREE }, // ér (兒)
	{ "老", TRANSPARENT, FREE }, // lǎo

	// jobs
	{ "师", TRANSPARENT, FREE }, // shī
	{ "卫", TRANSPARENT, FREE }, // wèi (衛)
	{ "工", TRANSPARENT, FREE }, // gōng
	{ "农", TRANSPARENT, FREE }, // nóng (農)
	{ "官", TRANSPARENT, FREE }, // guān
	{ "王", TRANSPARENT, FREE }, // wáng

	// animals
	{ "马", TRANSPARENT, FREE }, // mǎ (馬)
	{ "🏇", TRANSPARENT, FREE }, // 
	{ "虎", TRANSPARENT, FREE }, // hǔ
	{ "🐯", TRANSPARENT, FREE }, // 
	{ "猪", TRANSPARENT, FREE }, // zhū (豬)
	{ "🐷", TRANSPARENT, FREE }, // 
	{ "鸡", TRANSPARENT, FREE }, // jī (鳥)
	{ "鸟", TRANSPARENT, FREE }, // niǎo (鳥)
	{ "鱼", TRANSPARENT, FREE }, // yú (魚)
	{ "🐟", TRANSPARENT, FREE }, // 
	{ "羊", TRANSPARENT, FREE }, // yáng
	{ "🐏", TRANSPARENT, FREE }, // 
	{ "狼", TRANSPARENT, FREE }, // láng
	{ "🐺", TRANSPARENT, FREE }, // 


    // food
	{ "面", TRANSPARENT, FREE }, // miàn (麵)
	{ "啤", TRANSPARENT, FREE }, // pí
	{ "酒", TRANSPARENT, FREE }, // jiǔ
	{ "水", TRANSPARENT, FREE }, // shuǐ
	{ "米", TRANSPARENT, FREE }, // mǐ
	{ "粥", TRANSPARENT, FREE }, // zhōu
	{ "筷", TRANSPARENT, FREE }, // kuài

	//objects
	{ "书", TRANSPARENT, FREE}, // shū
};

// initialize elements (ncurses) colors pairs
void elements_colors_init() {
 for (unsigned int i = 0; i < ELT_COUNT; i++)
   init_pair(i+1, background_blocks[i].fgcolor, background_blocks[i].bgcolor);
}

background_block *get_background_blocks() {
  return background_blocks;
}

background_block *get_background_block( uint16_t number) {
  return &background_blocks[number];
}

// evaluate if a block if a block number, of this type.
bool is_background_bloc_of_type( uint16_t number, uint16_t type) {
  return background_blocks[number].type == type;
}

// =====
// logs
// should be in helper or log .c/.h
// =====

void log_init(char * LOGNAME) {
 LOGFILE = fopen( LOGNAME, "w");
}

int log_print(const char *fmt, ...) {
  va_list argp;
  va_start( argp, fmt);
  vfprintf( LOGFILE, fmt, argp);
  va_end( argp);
  fflush( LOGFILE);
};

int log_flush() {
  fflush( LOGFILE);
};

void log_close() {
  fclose( LOGFILE);
}



