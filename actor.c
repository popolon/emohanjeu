#include <stdlib.h>
#include "actor.h"

const uint8_t compass[] = {
  GE,  // 0  →
  GSE, // 1 ↘
  GS,  // 2 ↓
  GSW, // 3 ↙
  GW,  // 4 ←
  GNW, // 5 ↖
  GN,  // 6 ↑
  GNE  // 7 ↗
};
const char *name_compass[] = {
	"EAST",	"SOUTH-EAST", "SOUTH", "SOUTH-WEST",
	"WEST", "NORTH-WEST", "NORTH", "NORTH-EAST",
};

const uint16_t_vector direction_vector[8] = {
	{1,0}, //E
	{1,1}, //SE
	{0,1}, //S
	{-1,1}, //SW
	{-1,0}, //W
	{-1,-1}, //NW
	{0,-1}, //N
	{1,-1} //NE
};

const action_to_table action_cw[8] = {
 {GNE, 6}, //E
 {GNE, 7}, //SE
 {GSE, 6}, //S
 {GSE, 7}, //SW
 {GSW, 6}, //W
 {GSW, 7}, //NW
 {GNW, 6}, //N
 {GNW, 7}  //NE
};

const action_to_table action_ccw[8] = {
 {GSE, 6}, //E
 {GSW, 7}, //SE
 {GSW, 6}, //S
 {GNW, 7}, //SW
 {GNW, 6}, //
 {GNE, 7}, //NW
 {GNE, 6}, //N
 {GSE, 7}, //NE0
};

bool actor_go_if_can( actor_elt *actor, background_category tc, uint8_t direction) {
  log_print("actor_go_if_can start\n");

  uint16_t bg_width = actor->bg->width,
           bg_height = actor->bg->height;
  uint16_t vect_x = direction_vector[direction].x,
           vect_y = direction_vector[direction].y,
           new_x = actor->x + vect_x,
           new_y = actor->y + vect_y;

  log_print("actor_go_if_can test borders limit\n");
  // bound x && y :if != 0 
  if ( vect_x < 0 && new_x < 0) return false;
  else if ( vect_x > 0 && new_x >= bg_width) return false;
  if ( vect_y < 0 && new_y < 0) return false;
  else if ( vect_y > 0 && new_y >= bg_height) return false;

  // if it can move, then move
  log_print("actor_go_if_can move actor\n");
  if (background_tile_type_notbounded( actor->bg, new_x, new_y, tc)) {
    actor->x = new_x; actor->y = new_y;
    actor->direction = direction;
    return true;
  } else return false;
  log_print("actor_go_if_can start\n");
}

uint16_t length_to_border( actor_elt *actor, uint8_t direction, background_category tc) {
  uint16_t pos_x = actor->x,
           pos_y = actor->y,
           vect_x =  direction_vector[direction].x,
           vect_y =  direction_vector[direction].y,
           count = 0;

  bool touched = FALSE;
  while(!touched) {
	if( background_tile_type( actor->bg, pos_x += vect_x, pos_y += vect_y, tc)) {
      count++;
    } else
      return count;
  }
}

// search the shortest compass direction to the border on the current allowed path
uint8_t shortest_border_direction( actor_elt *actor, background_category tc) {
  uint8_t lowest, direction;
  uint16_t length, lowest_length = 65535;

  log_print("shortest_border_direction start\n----------------\n");

  for ( direction = 0; direction < 8; direction++) {
    length = length_to_border( actor, direction, tc);
    log_print("lenght (%d:%s) = %d\n", direction, name_compass[direction], length);
    if (length < lowest_length) {
      lowest = direction;
      lowest_length = length;
      log_print("direction (%d:%s) is now lowest\n", direction, name_compass[direction], length);
    }
  }
  log_flush();
  return direction;
}

// in case we reached a border case,
// search the first direction after 0 CW
// 00xx0xxx
//   ^  ^
bool first_non_border_direction_cw( actor_elt *actor, background_category tc) {
  uint8_t direction;
  bool initial_zero = FALSE,
       direction_zero = FALSE;
  
  uint16_t length;
  log_print("first_non_border_direction start\n----------------\n");

  for ( direction = 0; direction < 8; direction++) {
    length = length_to_border( actor, direction, tc);
    log_print("lenght (%d:%s) = %d\n", direction, name_compass[direction], length);
    if (initial_zero == FALSE) {
	  if( length == 0) {
        initial_zero = TRUE;
        log_print("direction (%d:%s) is initial_zero\n", direction, name_compass[direction]);
        if ( direction == 0 )
          direction_zero = TRUE; // warn that direzction 0 is at 0
      }
    } else { // already TRUE
      if( length > 0) { // found
		actor->direction = direction;
		return TRUE; 
	  }
    }
  }
  // finished first turn.
  if (initial_zero == FALSE) // no zero found, fail, not at border
    return FALSE;
  // we have 0 at the end, the first non-zero = first direction
  if ( direction_zero == FALSE ) { // direction 0 not at 0, OK
    actor->direction = 0;
    return TRUE;
  } else  // else means everything is at 0, I'm on an island
    return FALSE;
}


#define BYTE_TO_BINARY_PATTERN "%s%s%s%s%s%s%s%s"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? "" : ""), \
  (byte & 0x40 ? "" : ""), \
  (byte & 0x20 ? "ToBorder " : ""), \
  (byte & 0x10 ? "CW" : ""), \
  (byte & 0x08 ? "W" : ""), \
  (byte & 0x04 ? "E" : ""), \
  (byte & 0x02 ? "S" : ""), \
  (byte & 0x01 ? "N" : "") 

// is it near border (TRUE if yes)
bool actor_action_isat_border( actor_elt *actor, background_category tc) {
  log_print("actor_action_isat_border()\n");
// border of background is a border (=0 || max)
  uint8_t table[] = {
    0, 0, 0,
	0,    0,
	0, 0, 0 },
  count = 0;

  // this case use bits for the 8 possibilities, so
  // final result can be applied to a hash table to obtain
  // initial direction of the actor.
  // Bonus: if 0xFF => the neighbourgh is without border
  uint8_t tiles_around = 0;
  // bits are in circular order, so we can divide the relation table
  // in only 4 symetrics cases one of ~64 combination
  // and rotate the bitfield to match them instead of 256.
  // with horizontal symmetry, it could probably be limited  to 32 cases
  // 80 40 20
  // 01    10
  // 02 04 08
  if ( background_tile_type( actor->bg, actor->x -1, actor->y-1, tc)) // ↖
    tiles_around |= 0x80; // table[0] = 1; count++; }
  if ( background_tile_type( actor->bg, actor->x, actor->y -1, tc)) // ↑
    tiles_around |= 0x40;
  if ( background_tile_type( actor->bg, actor->x +1, actor->y -1, tc)) // ↗
    tiles_around |= 0x20;
  if ( background_tile_type( actor->bg, actor->x -1, actor->y, tc)) // ←
    tiles_around |= 0x01;
  if ( background_tile_type( actor->bg, actor->x +1, actor->y, tc)) // →	
    tiles_around |= 0x10;
  if ( background_tile_type( actor->bg, actor->x -1, actor->y+1, tc)) // ↙
    tiles_around |= 0x02;
  if ( background_tile_type( actor->bg, actor->x, actor->y +1, tc)) // ↓
    tiles_around |= 0x04;
  if ( background_tile_type( actor->bg, actor->x +1, actor->y+1, tc)) // ↘
    tiles_around |= 0x08;
  if ( tiles_around == 0xFF)
    return FALSE; // need to found the nearest border
  else
    return TRUE;
    //  find the good direction();
    // On prend le cas en haut à droite
}

// go straight on to border (TRUE if reached)
bool actor_action_goto_border( actor_elt *actor, background_category tc) {
  log_print("actor_action_goto_border start\n");
  if (actor_go_if_can( actor, tc, actor->direction))
    return TRUE;
  else
    return FALSE;
  log_print("actor_action_goto_border end\n");
}

// turn in a zone of a certain kind of category
void actor_action_follow_border( actor_elt *actor, background_category tc) {
  log_print("actor_action_follow_border\n");
  uint8_t base, max;

  // Test order and bounding :
  //
  //  4 3 2  x↗x  xxx  ←xxx
  //  5 ↑ 1  x1x  x↖1  7  x
  //  6 7         xxx
  // case 1 only occurs on diagonal directions so perpendicular start at 2
  // case 7 can occurs on any directions
  log_print("direction: %d .. sens: %d\n", actor->direction, actor->sens );

  if ( actor->sens == ACTOR_CLOCKWISE ) {
    base = action_cw[actor->direction].base_direction;
    max = action_cw[actor->direction].max_steps;
    for ( uint8_t i = 0; i < max; i++)
      if ( actor_go_if_can( actor, tc, compass[ (base + i) % 8] ) )
        return;
  } else {
    base = action_ccw[actor->direction].base_direction;
    max = action_ccw[actor->direction].max_steps;
    base += 8; // to avoid negative numbers
    for ( uint8_t i = 0; i < max; i++)
      if ( actor_go_if_can( actor, tc, compass[ (base - i) % 8] ) )
        return;
  }
}

uint8_t actor_action_follow_bg_path( actor_elt *actor, background_category tc) {
  // first : continue in the same direction
  // secondary : test nearest around
  // teartiary: test perpendicular

  // TODO: rendre plus générique
  
//  switch (actor->direction) {
//    case GO_EAST:
  if ( actor_go_if_can( actor, tc, GE )) return 1; // →
  else if ( actor_go_if_can( actor, tc, GNE )) return 1; // ↗
  else if ( actor_go_if_can( actor, tc, GSE )) return 1; // ↘
  else if ( actor_go_if_can( actor, tc, GN )) return 1; // ↑
  else if ( actor_go_if_can( actor, tc, GS )) return 1; // ↓
//  Didn't move
  return 0;
}

// follow a path predefinied in a list
void actor_action_follow_predefined_path( actor_elt *actor, uint16_t_vector *pathlist) {
}

void fish_action( actor_elt *actor) {
  // TODO: simple patch to loop fish, need to clean it 
  if ( actor->x + direction_vector[actor->direction].x >= actor->bg->width ) {
	actor->x = 7;
    actor->y = 10;
  }
  if ( actor_action_follow_bg_path( actor, BGTYPE_WATER) == 1 )
	actor->visible = 1;
  else if ( actor_action_follow_bg_path( actor, BGTYPE_BRIDGE) == 1 )
    actor-> visible = 0;
}

void fish_create( actor_elt *actor, background *bg) {
  actor->emoji = "🐟";
  actor->x = 7;
  actor->y = 10;
  actor->direction = GE;
  actor->direction = 0;
  actor->action_hook = &fish_action;
  actor->bg = bg;
  actor->visible = 1;
}

void wolf_action( actor_elt *actor) {
  log_print("wolf_action start\n");
//  if (in the middle of the forest=> test the whole cases around)
//  🌳🌳🌳
//  🌳🐺🌳 => find the minimal distance in all directions
//  🌳🌳🌳    then following straight on :
//    1. find and go to nearest border
//       or go to an arbitrary direction one
  //  actor_action_goto_border( actor, BGTYPE_WOOD);
//    2. When reached, set as initial condition, depending on cw/ccw,
//       the direction
//  🌳↖ 🌿
//  🌳🐺↘
//  🌳🌳🌳
// else
//   set as initial condition (depending on cw/ccw) the direction
//  background_category tc = BGTYPE_WOOD;

  if ( actor->state == A_INITIALSTATE ) {
//     if ( actor_action_isat_border( actor, BGTYPE_WOOD) ) {
//       actor->state = A_TURNAROUND;
//       actor->sens = ACTOR_CLOCKWISE;
//     } else {
       actor->direction = shortest_border_direction( actor, BGTYPE_WOOD);
       actor->state = A_GOTOBORDER;
//     }
  }
  log_print("wolf_action switch(actor->state)\n");

  switch (actor->state) { 
    case A_GOTOBORDER:
      if ( !actor_action_goto_border(actor, BGTYPE_WOOD)) { // can't go further
		  
		log_print("print foireux\n");
//		log_print("goto boder direction (%d:%s) at x:%u, y:%u\n", actor->direction,
//		           name_compass[actor->direction],
//		           actor->x, actor->y );
        actor->sens = ACTOR_CLOCKWISE;
		actor->state = A_TURNAROUND;
        if (first_non_border_direction_cw( actor, BGTYPE_WOOD)) { // go to that direction
		  if ( actor_go_if_can( actor, BGTYPE_WOOD, actor->direction) )
            return; // should work
	    } else { //can't move anymore
		  return; // something goes wrong 
	    }
	  }
      break;
    case A_TURNAROUND:
      actor_action_follow_border(actor, BGTYPE_WOOD);
      break;
  }
  log_print("wolf_action end\n");
}

void wolf_create( actor_elt *actor, background *bg) {
  actor->emoji = "🐺";
  actor->x = 9; // 9
  actor->y = 1; // 1
//  actor->x = 9; actor->y = 5;
//  actor->direction = 0;
  actor->state = A_INITIALSTATE;
  actor->direction = GS;
  actor->sens = ACTOR_CLOCKWISE;
  actor->action_hook = &wolf_action;
  actor->bg = bg;
  actor->visible = 1;
}

void lamb_action( actor_elt *actor) {
  log_print("lamb_action start\n");
  // some behavior
  log_print("lamb_action end\n");
}

void lamb_create( actor_elt *actor, background *bg) {
  actor->emoji = "🐑";
  actor->x = 16; // 9
  actor->y = 10; // 1
//  actor->x = 9; actor->y = 5;
//  actor->direction = 0;
  actor->state = A_INITIALSTATE;
  actor->direction = GS;
  actor->sens = ACTOR_CLOCKWISE;
  actor->action_hook = &lamb_action;
  actor->bg = bg;
  actor->visible = 1;
}

actor_elt *actor_create( uint16_t type, background *bg) {
  actor_elt *tmpactor;
  switch( type) {
	case ACTOR_TYPE_FISH :
      tmpactor = malloc(sizeof(actor_elt));
      if ( tmpactor != NULL)
        fish_create(tmpactor, bg);
      break;
    case ACTOR_TYPE_LAMB :
      tmpactor = malloc(sizeof(actor_elt));
      if ( tmpactor != NULL)
        lamb_create(tmpactor, bg);
      break;
    case ACTOR_TYPE_WOLF :
      tmpactor = malloc(sizeof(actor_elt));
      if ( tmpactor != NULL)
        wolf_create(tmpactor, bg);
      break;
    default:
      return NULL;
  }
  return tmpactor;
}

actor_elt *actor_delete( uint16_t type) {
  switch( type) {
	case ACTOR_TYPE_FISH :
      if ( fish != NULL)
         free(fish);
      break;
	case ACTOR_TYPE_LAMB :
      if ( lamb != NULL)
         free(lamb);
      break;
	case ACTOR_TYPE_WOLF :
      if ( wolf != NULL)
         free(wolf);
      break;
  }
}

// draw actor
void actor_draw( actor_elt *actor) {
  return;
}

// collision ?
bool actor_collision( actor_elt *actor, uint16_t x, uint16_t y) {
  return FALSE;
}

// consume object
bool actor_consume(actor_elt *actor, object_elt * object) {
  actor->score += object->object->score;
  if ( object->object->type == OBJECT_CALVAS ) {
	flash();
    actor->lives++;
  }
}

// actor own action
void actor_action( actor_elt *actor) {
  actor->action_hook(actor);
}
