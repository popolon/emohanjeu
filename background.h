#ifndef BACKGROUND_H
#define BACKGROUND_H
#include "element.h"

typedef struct background background;
struct background {
  uint16_t width;
  uint16_t height;
  uint16_t *tiles;
};

// return the tile at position x,y (no bound test)
uint16_t background_tile(background *bg, uint16_t x, uint16_t y);

// return the tile at position x,y (bound test, rare case)
uint16_t background_tile_bounded(background *bg, uint16_t x, uint16_t y);


// is the tile at position x,y of type (test background bounds)
bool background_tile_type(background *bg, uint16_t x, uint16_t y, uint16_t type);

// is the tile at position x,y of type (don't test background bounds, rare)
bool background_tile_type_notbounded(background *bg, uint16_t x, uint16_t y, uint16_t type);

// is the tile at position x,y blocking
bool background_tile_blocking(background *bg, uint16_t x, uint16_t y);

#endif /* BACKGROUND_H */
