#ifndef ACTOR_H
#define ACTOR_H

// actor need to know which element it interact in playground
#include "background.h"


// ***** MOVMENT *****
// action order, clockwise
enum action_order { GE, GSE, GS, GSW, GW, GNW, GN, GNE };

enum action_state { A_INITIALSTATE, A_GOTOBORDER, A_TURNAROUND };

#define ACTOR_GONORTH 1
#define ACTOR_GOSOUTH 2
#define ACTOR_GOEAST 4
#define ACTOR_GOWEST 8

enum action_sens { ACTOR_CLOCKWISE, ACTOR_UP, ACTOR_DOWN};

typedef struct action_to_table action_to_table;
struct action_to_table {
  uint8_t base_direction;
  uint8_t max_steps;
};

typedef struct uint16_t_vector uint16_t_vector;
struct uint16_t_vector {
	uint16_t x;
	uint16_t y;
};

// ***** definitions *****
typedef struct actor_type actor_type;
struct actor_type {
  char *emoji;
//  func *algoAI;
  uint32_t PV;
  uint32_t PVscore;
  uint32_t XP;
};

/*
 * typedef struct actor_elt actor_elt;
struct actor_elt {
  char *emoji;
//  actor_function *func;
  uint32_t x;
  uint32_t y;
};
*/

typedef struct actor_elt actor_elt;
struct actor_elt {
  char *emoji;
  attr_t attr_nc; // ncurses window print attributes
  char *name;
  uint16_t x;
  uint16_t y;
  uint8_t lives;
  uint32_t score;
  uint8_t state; // current state/action
  uint8_t direction; // n↑ e→ s↓ w←
  uint8_t sens; // (cw | ccw) | (in | out)
  uint8_t but;  // gotoborder | follow border | follow path
  uint8_t visible;
  background *bg;         // current background
  void (* action_hook)(actor_elt *actor); // 	evoke at each turn
/*  uint32_t XP;
  uint32_t dexterity;
  uint32_t speed;
  uint32_t force;
  uint32_t weight;
  uint32_t height;
  uint32_t endurance;
  uint32_t IQ;
  void *iventory; */
};

// ***** zoo *****
enum actor_type_enum {
  ACTOR_TYPE_FISH,
  ACTOR_TYPE_PIG,
  ACTOR_TYPE_LAMB,
  ACTOR_TYPE_WOLF
};

// pour le dev initial, passer tout ça par arguments/table ensuite
static actor_elt *fish;
static actor_elt *lamb;
static actor_elt *wolf;

// ***** functions *****

// search the shortest compass direction to the border on the current allowed path
uint8_t shortest_border_direction( actor_elt *actor, background_category tc);

// is it near border (TRUE if yes)
bool actor_action_isat_border( actor_elt *actor, background_category tc);

// go straight on to border (TRUE if reached)
bool actor_action_goto_border( actor_elt *actor, background_category tc);

// turn in a zone of a certain kind of category
void actor_action_follow_border(actor_elt *actor, background_category tc);

// follow a path on a certain kind of category TODO: a list of categories
uint8_t actor_action_follow_bg_path(actor_elt *actor, background_category tc);

// follow a path predefinied in a list
void actor_action_follow_predefined_path(actor_elt *actor, uint16_t_vector *pathlist);

// crate actor(s?)
actor_elt *actor_create(uint16_t type, background *bg);

// TODO: doit retourner la position actuelle et le sprite à afficher
// draw actor
void actor_draw(actor_elt *actor);

// collision ?
bool actor_collision(actor_elt *actor, uint16_t x, uint16_t y);

// consume object
bool actor_consume(actor_elt *actor, object_elt * object);

// actor own action
void actor_action(actor_elt *actor);

#endif /* ACTOR_H  */
