#include "playground.h"

// position of the playground window in the background
uint16_t pg_x;
uint16_t pg_y;
WINDOW *pg_win;


// level courant
level_elt *current_level_elt;

// background du level
background *bg;

// infos du bg
uint16_t level_w,
         level_h,
         *level_t;

// list of actors
actor_elt **actors_queue;

background_block *blocks;


WINDOW *playground_window_create( level_elt *level) {
  // newwin(height, width, starty, startx);
  pg_win = newwin(PGV_HEIGHT, PGV_WIDTH * 2, 1, 0);

  
  // get the current blockset /*fixme should be take by playground  to the level instead ? */
  blocks = get_background_blocks();

  // TODO: 0 par défaut pour le moment, doit être récupéré dans level
  // chargé
  pg_x = 0;
  pg_y = 0;

  log_print("playground_window_create start\n");

  log_print("playground_window_create level adr = %x (adr): %x\n", (long int) level, (long int) &level);
  log_print("playground_window_create level->bg adr = %x\n", (long int) level->bg);

  current_level_elt =  level;
  bg = level->bg;
  
  log_print("playground_window_create : current_background set\n");
  log_print("playground_window_create : background: w=%d, h=%d\n", bg->width, bg->height);

   // pour raccourcir
  level_w = bg->width;
  level_h = bg->height;
  level_t = bg->tiles;

  return pg_win;
}

void playground_window_delete() {
 delwin( pg_win);
}

void playground_actors_add(actor_elt **actors) {
  actors_queue = actors;
}

// move the playground window inside the background
void playground_move(uint16_t x, uint16_t y) {
   // TODO test des limites du bg.
   pg_x += x;
   pg_y += y;
}

// get the current playground relative tile (playgound uperleft corner position in background + coord of tile in playground)  
uint16_t playground_tile(uint16_t x, uint16_t y) {
  return level_t[ (pg_y + y) * level_w + (pg_x + x)];
}

// draw the tile at position x,y, WARNING, us playground_tile that manage playground position in background
uint16_t playground_tile_draw(uint16_t x, uint16_t y) {
  uint16_t di = playground_tile(x, y);
  wattron( pg_win, COLOR_PAIR(di+1));
  mvwprintw( pg_win, y, x * 2,  blocks[di].hanzi);
  wattroff( pg_win, COLOR_PAIR(di+1));
}

void playground_window_fill() {
  // TODO: manage playground_x/y position
  uint16_t dx,dy,di;
  for ( dy = 0; dy < level_h; dy++)
    for ( dx = 0; dx < level_w; dx++) {
      di = playground_tile(dx, dy);
      wattron( pg_win, COLOR_PAIR(di+1));
      mvwprintw( pg_win, dy, dx * 2, blocks[di].hanzi);
      wattroff( pg_win, COLOR_PAIR(di+1));
    }
}

void playground_objects_draw() {
  log_print("playground_objects_draw start\n");
  log_print("playground_objects_draw level adr = %x (adr): %x\n", (long int) current_level_elt, (long int) &current_level_elt);

  uint16_t nbr_obj = current_level_elt->object_count;
  
  log_print("playground_objects_draw : nbr_obj = %u\n", nbr_obj);
  log_flush();

  struct object_elt *obj = current_level_elt->objects;
  // objects use whole background coordinates, need to substract playground position
  for ( uint16_t i = 0; i < nbr_obj; i++)
    if ( obj[i].here == TRUE)
      mvwprintw( pg_win, obj[i].y - pg_y, (obj[i].x - pg_x) * 2, obj[i].object->emoji);
}

uint32_t playground_object_collide( uint16_t x, uint16_t y, actor_elt *actor) {
  uint16_t nbr_obj = current_level_elt->object_count;
  struct object_elt *obj = current_level_elt->objects;
  for ( uint16_t i = 0; i < nbr_obj; i++)
    if( obj[i].here == TRUE && x == obj[i].x && y == obj[i].y) {
      obj[i].here = FALSE;
      actor_consume(actor, &obj[i]);
    }
  return 0;
}

// TODO: need to change it to a parsing of list of playground actors
// Is there already a character at this position
actor_elt *playground_character_collide(uint16_t x, uint16_t y, actor_elt *actor) {
  if ( actor->x == x && actor->y == y ) {
    return actor;
  } else {
    return NULL;
  }
}

//TODO : manage remove of preview position ?
// draw an actor linked to the current level
void playground_actor_draw(actor_elt *actor) {
  uint16_t x = actor->x;
  uint16_t y = actor->y;
  uint16_t pgx = x - pg_x; // substract pg position on bg.
  uint16_t pgy = y - pg_y;
  // eliminate out of playground view actors
  if ( pgx < 0 || pgx >= PGV_WIDTH)
    return;
  if ( pgy < 0 || pgy >= PGV_HEIGHT)
    return;
  if ( actor->visible == 1)
    mvwprintw( pg_win, pgy, pgx * 2, actor->emoji);
//  mvwprintw(playgroundwin, 0, 0, "pgx: %d, pgy: %d", pgx, pgy);
}

// draw actors linked to the current level
void playground_actors_draw() {
	// for actor in actors => playground_actor_draw(actor)
}
