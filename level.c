#include "level.h"

object_block cerise = { "🍒", 20, 1, 0, OBJECT_CHERRY};
object_block strawb = { "🍓", 50, 1, 0, OBJECT_STRAWBERRY};
object_block apple = { "🍎", 100, 1, 0, OBJECT_APPLE};
object_block ricebowl = { "🍚", 300, 1, 0, OBJECT_RICE};
object_block noodles = { "🍜", 300, 1, 0, OBJECT_NOODLES};
object_block bier = { "🍺", 500, 2, 0, OBJECT_BIER};
object_block ganbei = { "🍻", 100, 2, 0, OBJECT_GANBEI};
object_block wine = { "🍷", 800, 2, 0, OBJECT_WINE};
object_block calvas = { "🥃", 1600, 2, 0, OBJECT_CALVAS};

object_elt level1_obj[] = {
	{&strawb, 3, 0, true},
	{&cerise, 8, 7, true},
	{&cerise, 7, 4, true},
	{&apple, 9, 3, true},
	{&ricebowl, 16, 5, true}, // 5

	{&noodles, 19, 8, true},
	{&bier, 19, 3, true},
	{&ganbei, 20, 9, true},
	{&calvas, 18, 7, true} // 9
};

// positions initiales des acteurs version simplifiée et static
// de d'actor_elt (type, position, direction initiales)
// ------------------
initial_actor level1_act[] = {
 {ACTOR_TYPE_FISH, GE,  7, 10},
 {ACTOR_TYPE_WOLF, GS,  9,  5},
 {ACTOR_TYPE_LAMB, GS, 16, 10}
};

//wchar_t nan = L'男';
// 山 6
// 草 0
// 森 1
// 林 2
// 河 8
// 城 14
// 家 17
// 桥 16
// 山山山山山山草草草草森林草草草草草草草草草草草草河
// 山山山山草草草草草森森森森草草草草城城城城城草河草
// 山山山草草草草森森森森森森草草城城家城家城城河草草
// 山山草草草草草森森森森森森草草城城城城城城河城草草
// 山山草草草森森森森森森森草草草城城家城酒河城城草草
// 山山山山草草森森森森森森草草草城城河桥河城城草草草
// 山山山草草草森森森森森草草草草河河城城城城城草草草
// 山山草草草草草森森森草草草河河草城城城城城草草林林
// 山山草草草草森森森森草河河草草草草城城城草草草林林
// 草草草草草草森森草河河草草草草草草草草草草草林林林
// 草草草草草草草河河草草草草草草草草草草草草草草林林

uint16_t level1_background[] = { 5, 5, 5, 5, 5, 5, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 
5, 5, 5, 5, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 14, 14, 14, 14, 14, 0, 8, 0, 
5, 5, 5, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 14, 14, 17, 14, 17, 14, 14, 8, 0, 0, 
5, 5, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 14, 14, 14, 14, 14, 14, 8, 14, 0, 0, 
5, 5, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 14, 14, 17, 14, 18, 8, 14, 14, 0, 0, 
5, 5, 5, 5, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 14, 14, 8, 16, 8, 14, 14, 0, 0, 0, 
5, 5, 5, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 8, 8, 14, 14, 14, 14, 14, 0, 0, 0, 
5, 5, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 8, 8, 0, 14, 14, 14, 14, 14, 0, 0, 2, 2, 
5, 5, 0, 0, 0, 0, 1, 1, 1, 1, 0, 8, 8, 0, 0, 0, 0, 14, 14, 14, 0, 0, 0, 2, 2, 
0, 0, 0, 0, 0, 0, 1, 1, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 
0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2 };

static background level1_bg = {LEVEL1_BG_W, LEVEL1_BG_H, level1_background};

static level_elt level1_elt = {
	&level1_bg,
	9,
	level1_obj,
	3,
	level1_act
};

uint16_t level2_background[] = {0, 0};

uint16_t *levels[] = { level1_background, level2_background };

// utilise en dur pour le moment
level_elt *levels_elt[] = { &level1_elt };


level_elt *level_init(uint16_t level) {
 // initialise background
 
 // initalise objets
 
 // initialise actors
 return &level1_elt;
}


// récupère un level_elt
level_elt *get_level_elt(uint16_t level) {
  return  &level1_elt;
}

// récupère un level_elt
background *get_level_bg(uint16_t level) {
  return  level1_elt.bg;
}

