#ifndef LEVEL_H
#define LEVEL_H

#include "element.h"
#include "background.h"
#include "actor.h"

// version limité d'un acteur pour définition dans level

typedef struct initial_actor initial_actor;
struct initial_actor {
  uint8_t type;
  uint8_t direction;
  uint16_t pos_x;
  uint16_t pos_y;
};

typedef struct level_elt level_elt;
struct level_elt {
  background *bg;
  uint16_t object_count;
  struct object_elt *objects;
  uint16_t actor_count;
  struct initial_actor *actors;
};

#define LEVEL1_BG_W 25
#define LEVEL1_BG_H 11 
#define LEVEL1_BG_S LEVEL1_BG_W * LEVEL1_BG_H

// Initialisation et retourne level_elt
level_elt *level_init( uint16_t level);

// récupère un level_elt
level_elt *get_level_elt( uint16_t level);

// récupère un level_bg
background *get_level_bg( uint16_t level);

#endif /* LEVEL_H  */
