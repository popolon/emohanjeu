#ifndef PLAYGROUND_H
#define PLAYGROUND_H
// playground is the viewport of the game playground itself
// including background, objects and actors

#include <ncurses.h>

#include "level.h"

#define PGV_WIDTH 25
#define PGV_HEIGHT 11

// constructor
WINDOW *playground_window_create( level_elt *level);
void playground_window_delete();

// move the playground window inside the background
void playground_move(uint16_t x, uint16_t y);

// get the current playground relative tile (playgound uperleft corner position in background + coord of tile in playground)  
uint16_t playground_tile( uint16_t x, uint16_t y);

// draw the tile at position x,y, WARNING, us playground_tile that manage playground position in background
uint16_t playground_tile_draw( uint16_t x, uint16_t y);

// fill the playground window with currentlevel
void playground_window_fill();

// draw objects linked to the current level
void playground_objects_draw();

// does have a collision with an object in the current x,y coordinates
// x and y can be different from current object for evaluate moves
uint32_t playground_object_collide( uint16_t x, uint16_t y, actor_elt *actor);

// does have a collition with another character
actor_elt *playground_character_collide(uint16_t x, uint16_t y, actor_elt *actor);

// draw an actor linked to the current level
void playground_actor_draw( actor_elt *actor);

// draw actors linked to the current level
void playground_actors_draw();


#endif /* PLAYGROUND_H */
