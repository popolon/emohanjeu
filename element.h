#ifndef ELEMENT_H
#define ELEMENT_H

#include <stddef.h>
#include <stdbool.h>
#include <locale.h>
#include <ncurses.h>

//uint8_t debug_display;

// #define LIVE L'💌'
#define LIVE "💌"
#define LIVE2 "🧬"

enum bgtype_enum {
  BGTYPE_FLAT,
  BGTYPE_WOOD,
  BGTYPE_MOUNT,
  BGTYPE_ROCK,
  BGTYPE_WATER,
  BGTYPE_BLOCKER,
  BGTYPE_BUILDING,
  BGTYPE_BRIDGE
};

enum object_type_enum {
  OBJECT_CHERRY,
  OBJECT_STRAWBERRY,
  OBJECT_APPLE,
  OBJECT_RICE,
  OBJECT_NOODLES,
  OBJECT_BIER,
  OBJECT_GANBEI,
  OBJECT_WINE,
  OBJECT_CALVAS
};

typedef uint16_t background_category;

static uint16_t level_cur;

typedef struct background_block background_block;
struct background_block {
  char *hanzi;
  uint32_t fgcolor;
  uint32_t bgcolor;
  uint16_t type;
};

typedef struct element_block element_block;
struct element_block {
  char *hanzi;
  uint32_t fgcolor;
  uint32_t bgcolor;
  uint16_t type;
};

typedef struct object_block object_block;
struct object_block {
  char *emoji;
  uint32_t score;
  uint32_t bonus;
  uint32_t malus;
  uint32_t type;
};

// x ou y en premier plutôt qu'emoji, pour scan ?
typedef struct object_elt object_elt;
struct object_elt {
  struct object_block *object;
  uint32_t x;
  uint32_t y;
  bool here;
};

#define ELT_COUNT 21

// initialize elements (ncurses) colors pairs
void elements_colors_init();

void log_init(char *LOGNAME);
int log_print(const char *fmt, ...);
int log_flush();
void log_close();

#define WHITE 0xFFFFFFFF
#define LIGHTGRAY 0xFFCCCCCC
#define GRAY 0xFF888888
#define DEEPGRAY 0xFF444444

#define RED 0xFFFF0000
#define LIGHTRED 0xFFFF8888
#define DEEPRED 0xFF880000

#define GREEN 0xFF00FF00
#define LIGHTGREEN 0xFF88FF88
#define DEEPGREEN 0xFF008800

#define BLUE 0xFF0000FF
#define LIGHTBLUE 0xFF8888FF
#define DEEPBLUE 0xFF000088

#define YELLOW 0xFF00FFFF
#define LIGHTYELLOW 0xFF88FFFF
#define DEEPYELLOW 0xFF008888

#define PINK 0xFFFF00FF
#define LIGHTPINK 0xFFFF88FF
#define DEEPPINK 0xFF880088

#define KAKI 0xFF88FFCC

#define BROWN 0xFFAA8833
#define LIGHTBROWN 0xFFFFCC88

#define TRANSPARENT 0x000000
#define FREE 0x00FFFFFF


// return the list of background_blocks
background_block *get_background_blocks();

// returne a specific background_block depending on it's number
background_block *get_background_block( uint16_t number);

// evaluate if a block if a block number, of this type.
bool is_background_bloc_of_type( uint16_t number, uint16_t type);


#endif /* ELEMENT_H  */
