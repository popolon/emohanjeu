#include <ncurses.h>
#include <locale.h>

int main(int argc, char *argv[])
{
        setlocale(LC_ALL, "");
        initscr();                      /* Start curses mode            */
	printw("utf8 ncurses 你好\n");
//        add_wch(L"好");
        attron(A_DIM);
        printw("a_dim好\n");
 	attroff(A_DIM);
	printw("a normal好\n");
	attron(A_BOLD);
	printw("a_bold好\n");
	attroff(A_BOLD);
	attron(WA_HORIZONTAL);
	printw("WA_HORIZONTAL\n");
	attroff(WA_HORIZONTAL);
	attron(A_STANDOUT);
	printw("A_STANDOUT 🍥\n");
	attroff(A_STANDOUT);
	printw("A_STANDOUT 🍥\n");
    	getch();
	endwin();
        printf("utf8 non-ncurses 你好\n");
}
